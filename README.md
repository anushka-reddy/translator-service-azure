# Translator Service Azure

This repository contains a Flask application for a simple language translation service for users who belong to a certain AD group. Those users can input text, select a desired language, and only receive the translated text using the Azure Translator Text API.

## Files Included

1. **app.py**: This file contains the Flask application code. It handles the routing for the homepage, form submission, and translation functionality. It utilizes the Azure Translator Text API for text translation.

2. **index.html**: This HTML file serves as the homepage template. It includes a form where users can input text and select the desired language for translation.

3. **results.html**: This HTML file displays the results of the translation. It shows the original text, the translated text, and the target language. Additionally, it provides a button to translate more text.

## Setup Instructions

To run this application locally, follow these steps:

1. Clone the repository to your local machine.

2. Install the required Python packages by running:
```bash
pip install flask requests python-dotenv
```
3. Sign up for the Azure Translator Text API and obtain your subscription key and service endpoint.

4. In the `.env` file in the root directory of the project, add the following environment variables:
KEY=your_subscription_key
ENDPOINT=your_service_endpoint
LOCATION=your_service_region

5. Run the Flask application by executing:
```bash
python app.py
```

6. Open a web browser and navigate to `http://localhost:5000` to access the language translation service.

## Usage

1. Input the text you want to translate into the provided text area.

2. Select the desired language from the dropdown menu.

3. Click the "Translate!" button to submit the form.

4. The translated text will be displayed on the results page along with the original text and the target language.

5. To translate more text, click the "Translate More" button.

## Example
![Example of program, deployed in Azure](Screen_Shot_2024-03-02_at_10.29.08.png)
![Result of program, deployed in Azure](Screen_Shot_2024-02-28_at_22.33.12.png)

## Technologies Used

- Python
- Flask
- HTML
- Azure Translator Text API
- Azure AD

## Contributors

This repository is maintained by Anushka Reddy.

Feel free to contribute by submitting pull requests or reporting issues.

For questions or support, please contact anushkar614@gmail.com.
