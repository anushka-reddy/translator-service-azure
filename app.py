from flask import Flask, render_template, request
import requests
import os
import uuid
from dotenv import load_dotenv

# Initialize Flask app
app = Flask(__name__)

# Load environment variables
load_dotenv()

# Homepage route
@app.route('/', methods=['GET'])
def home():
    # Display the homepage
    return render_template('index.html')

# Handle form submission
@app.route('/', methods=['POST'])
def translate_text():
    # Extract form data
    text_to_translate = request.form.get('text')
    desired_language = request.form.get('language')

    # Get environment variables
    subscription_key = os.getenv('KEY')
    service_endpoint = os.getenv('ENDPOINT')
    service_region = os.getenv('LOCATION')

    # Define API URL and parameters
    api_url = f"{service_endpoint}/translate?api-version=3.0&to={desired_language}"

    # Set request headers
    request_headers = {
        'Ocp-Apim-Subscription-Key': subscription_key,
        'Ocp-Apim-Subscription-Region': service_region,
        'Content-Type': 'application/json',
        'X-ClientTraceId': str(uuid.uuid4())
    }

    # Request body
    request_body = [{'text': text_to_translate}]

    # Send POST request to the translation API
    response = requests.post(api_url, headers=request_headers, json=request_body)

    # Parse the response
    translation_results = response.json()
    translation = translation_results[0]['translations'][0]['text']

    # Display the results
    return render_template('results.html', 
                           translated_text=translation, 
                           original_text=text_to_translate, 
                           target_language=desired_language)
